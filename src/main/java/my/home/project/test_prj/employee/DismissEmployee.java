package my.home.project.test_prj.employee;

import my.home.project.test_prj.ListDB;

import java.util.Scanner;

public class DismissEmployee {
    public static void dismissEmployee (){
        Scanner scanner = new Scanner(System.in);
        InfoEmployee.showInfo();
        System.out.println("Enter ID you wont to dismiss:  1 - " + ListDB.getListEmployee().size());
        int id = scanner.nextInt() - 1;
        ListDB.getListEmployee().get(id).setFired(true);
        System.out.println("Employee dismissed");
    }
}
