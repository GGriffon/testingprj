package my.home.project.test_prj.employee;

import my.home.project.test_prj.ListDB;

import java.util.ArrayList;
import java.util.Collections;

public class SortEmployee {
    public static void sortEmployee() {
        ArrayList<Employee> sortedEmployee = new ArrayList<>(ListDB.getListEmployee());
        Collections.sort(sortedEmployee);
        for (Employee e : sortedEmployee) {
            System.out.println(e.getId() + " " + e.getCreationDate() + " "
                    + e.getModifyDate() + " " + e.getName() + " " + e.getSurname() +
                    " " + e.getFsurname() + " " + e.getPosition());
        }
    }
}
