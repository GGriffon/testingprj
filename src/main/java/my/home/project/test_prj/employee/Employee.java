package my.home.project.test_prj.employee;

import java.time.LocalDate;
import java.time.LocalTime;

public class Employee implements Comparable<Employee> {

    int id;
    LocalDate creationDate;
    LocalTime modifyDate;
    String name;
    String surname;
    String fsurname;
    String position;
    boolean fired;

    public Employee(int id, LocalDate creationDate, LocalTime modifyDate, String name, String surname, String fsurname, String position, boolean fired) {
        this.id = id;
        this.creationDate = creationDate;
        this.modifyDate = modifyDate;
        this.name = name;
        this.surname = surname;
        this.fsurname = fsurname;
        this.position = position;
        this.fired = fired;
    }

    public Employee() {
    }


    public int getId() {
        return id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setModifyDate(LocalTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public LocalTime getModifyDate() {
        return modifyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFsurname() {
        return fsurname;
    }

    public void setFsurname(String fsurname) {
        this.fsurname = fsurname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isFired() {
        return fired;
    }

    public void setFired(boolean fired) {
        this.fired = fired;
    }

    @Override
    public int compareTo(Employee o) {
        return this.getSurname().compareTo(o.getSurname());
    }
}

