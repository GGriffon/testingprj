package my.home.project.test_prj.employee;

import my.home.project.test_prj.ListDB;
import my.home.project.test_prj.to_json.ConvertToJSON;

public class InfoEmployee {
    public static void showInfo() {
        int count = 0;
        for (Employee e : ListDB.getListEmployee()) {
            count++;
            System.out.println(count + " " + ConvertToJSON.toJSON(e));
        }
    }
}
