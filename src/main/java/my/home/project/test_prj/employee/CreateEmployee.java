package my.home.project.test_prj.employee;

import my.home.project.test_prj.ListDB;
import my.home.project.test_prj.position.CreatePosition;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class CreateEmployee {
    private static int count = 0;
    //String userInput = "";

    public static void createEmployee() {
        ArrayList<Employee> employeeArrayList = ListDB.getListEmployee();
        count++;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Name");
        String name = scanner.next();
        System.out.println("Enter Surname");
        String surname = scanner.next();
        System.out.println("Enter FSurname");
        String fsurname = scanner.next();
        System.out.println("Enter JT");
        String position = scanner.next();
        Employee employee = new Employee(count, LocalDate.now(), null, name, surname, fsurname, position, false);
        employeeArrayList.add(employee);
        System.out.println("User created ");
        CreatePosition.createPosition(employee.getPosition());
    }
}
