package my.home.project.test_prj.employee;

import my.home.project.test_prj.ListDB;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

public class ModifyEmployee {
    public static void modifyEmployee() {
        ArrayList<Employee> employees = ListDB.getListEmployee();
        Scanner scanner = new Scanner(System.in);
        if (employees.isEmpty()) {
            System.out.println("list is empty");
        } else {
            InfoEmployee.showInfo();
            System.out.println("Enter ID you wont to change:  1 - " + employees.size());
            int id = scanner.nextInt() - 1;

            System.out.println("Enter new surname ");
            String changeEmployee = scanner.next();
            employees.get(id).setSurname(changeEmployee);
            employees.get(id).setModifyDate(LocalTime.now());
            System.out.println("User replace");
        }
    }
}

