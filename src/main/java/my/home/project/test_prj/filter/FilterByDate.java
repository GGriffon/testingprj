package my.home.project.test_prj.filter;

import my.home.project.test_prj.employee.Employee;
import my.home.project.test_prj.to_json.ConvertToJSON;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class FilterByDate {
    public static void filterByDate(ArrayList<Employee> eList){
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("d/M/yyyy");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a date after (like d/m/yyyy) ");
        String inputDateAfter = scanner.next();
        System.out.println("Enter a date before (like d/m/yyyy) ");
        String inputDateBefore = scanner.next();
        LocalDate dateAfter = LocalDate.parse(inputDateAfter, dateFormat);
        LocalDate dateBefore = LocalDate.parse(inputDateBefore, dateFormat);
        for (Employee e : eList){
            if (e.getCreationDate().isAfter(dateAfter) && e.getCreationDate().isBefore(dateBefore)){
                System.out.println(ConvertToJSON.toJSON(e));
            }
        }
    }
}
