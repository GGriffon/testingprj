package my.home.project.test_prj.filter;

import my.home.project.test_prj.employee.Employee;
import my.home.project.test_prj.ListDB;

import java.util.ArrayList;
import java.util.Scanner;

public class FilterEmployee {
    public static void filterEmployee() {
        ArrayList<Employee> employeeList = ListDB.getListEmployee();
        Scanner scanner = new Scanner(System.in);
        String userInput = "";
        while (!userInput.equals("Q")) {
            System.out.println("Q - quit, N - name, D - date, P - position");
            userInput = scanner.next();
            userInput = userInput.toUpperCase();

            switch (userInput) {
                case "Q":
                    break;
                case "N":
                    FilterByName.filterByName(employeeList);
                    break;
                case "D":
                    FilterByDate.filterByDate(employeeList);
                    break;
                case "P":
                    FilterByPosition.filterByPosition(employeeList);
                    break;
                default:
                    System.out.println("Wrong pick");
            }
        }
    }
}
