package my.home.project.test_prj.filter;

import my.home.project.test_prj.employee.Employee;
import my.home.project.test_prj.to_json.ConvertToJSON;

import java.util.ArrayList;
import java.util.Scanner;

public class FilterByName {
    public static void filterByName(ArrayList<Employee> eList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter user name ");
        String inputName = scanner.next();
        for (Employee e : eList){
            if (e.getName().contains(inputName) || e.getSurname().contains(inputName) ){
                System.out.println(ConvertToJSON.toJSON(e));
            }
        }
    }
}
