package my.home.project.test_prj.filter;

import my.home.project.test_prj.employee.Employee;
import my.home.project.test_prj.to_json.ConvertToJSON;

import java.util.ArrayList;
import java.util.Scanner;

public class FilterByPosition {
    public static void filterByPosition(ArrayList<Employee> eList){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter position");
        String inputPosition = scanner.next();
        for (Employee e : eList){
            if (e.getPosition().contains(inputPosition)){
                System.out.println(ConvertToJSON.toJSON(e));
            }
        }
    }
}
