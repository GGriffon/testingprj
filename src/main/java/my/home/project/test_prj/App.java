package my.home.project.test_prj;

import my.home.project.test_prj.employee.Employee;

import java.time.LocalDate;

public class App
{
    public static void main( String[] args ) {

        ListDB.getListEmployee().add(new Employee(1, LocalDate.now(), null, "name", "surname", "fsurname", "position", false));
        ListDB.getListEmployee().add(new Employee(2, LocalDate.now(), null, "qqq", "ssaaa", "qqq", "qqq", false));
        ListDB.getListEmployee().add(new Employee(3, LocalDate.now(), null, "sss", "sss", "sss", "sss", false));
        ListDB.getListEmployee().add(new Employee(4, LocalDate.now(), null, "aaa", "aaa", "aaa", "aaa", false));
        ListDB.getListEmployee().add(new Employee(5, LocalDate.now(), null, "sss", "aaa", "zzz", "aaa", false));
        ListDB.getListEmployee().add(new Employee(6, LocalDate.now(), null, "zzz", "zzz", "aaa", "aaa", false));

        Control.control();

    }
}
