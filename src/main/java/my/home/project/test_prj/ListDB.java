package my.home.project.test_prj;

import my.home.project.test_prj.employee.Employee;
import my.home.project.test_prj.position.Position;

import java.util.ArrayList;

public class ListDB {
    static ArrayList<Position> listPosition = new ArrayList<>();
    static ArrayList<Employee> listEmployee = new ArrayList<>();

    public static ArrayList<Position> getListPosition() {
        return listPosition;
    }

    public static ArrayList<Employee> getListEmployee() {
        return listEmployee;
    }
}
