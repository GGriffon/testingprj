package my.home.project.test_prj;

import java.util.Scanner;

public class Control {
    public static void control() {
        Scanner scanner = new Scanner(System.in);
        String userInput = "";

        ControlEmployee controlEmployee = new ControlEmployee();

        ControlPosition controlPosition = new ControlPosition();
        while (!userInput.equals("Q")) {
            System.out.println("Make you chose E or P \nQ - for quit");
            userInput = scanner.next();
            userInput = userInput.toUpperCase();

            switch (userInput) {
                case "Q": {
                    System.out.println("Работа завершина");
                    break;
                }
                case "E": {
                    controlEmployee.controlEmployee();
                    break;
                }
                case "P": {
                    controlPosition.controlPosition();
                    break;
                }
                default:
                    System.out.println("Chose from E or P");
            }
        }
    }
}
