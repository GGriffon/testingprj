package my.home.project.test_prj.position;

import my.home.project.test_prj.ListDB;
import my.home.project.test_prj.dao.PostionDao;
import my.home.project.test_prj.to_json.ConvertToJSON;

public class InfoPosition {
    public static void showInfo() {
        PostionDao postionDao = new PostionDao();
        if (postionDao.geAll().isEmpty()) {
            System.out.println("List is empty");
        }
        for (Position p : postionDao.geAll()) {
            System.out.println(p.getId() + " " + ConvertToJSON.toJSON(p));
        }
    }
}

