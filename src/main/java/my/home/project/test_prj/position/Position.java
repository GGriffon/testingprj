package my.home.project.test_prj.position;

public class Position {
    private int id;
    private String jobTitle;

    public Position(int id, String jobTitle) {
        this.id = id;
        this.jobTitle = jobTitle;
    }
    public Position(){}

    public int getId() {
        return id;
    }
    public String getJobTitle() {
        return jobTitle;
    }
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
}
