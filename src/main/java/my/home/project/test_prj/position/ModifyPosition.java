package my.home.project.test_prj.position;

import my.home.project.test_prj.ListDB;
import my.home.project.test_prj.dao.PostionDao;

import java.util.ArrayList;
import java.util.Scanner;

public class ModifyPosition {
    public static void modifyPosition(){
        PostionDao positionDao = new PostionDao();
        Scanner scanner = new Scanner(System.in);
        if (positionDao.geAll().isEmpty()) {
            System.out.println("list is empty");
            return;
        }

        InfoPosition.showInfo();
        System.out.println("Enter ID you wont to change:  1 - " + positionDao.geAll().size());
        int id = scanner.nextInt() - 1;

        System.out.println("Enter new JT");
        String njt = scanner.next();

        if (positionDao.geAll().stream().noneMatch(position -> position.getJobTitle().contains(njt))) {
            System.out.println("user " + positionDao.geAll().get(id).getJobTitle() + " replace with " + njt);
            Position position = positionDao.geAll().get(id);

            positionDao.update(position, new String[]{njt});
        } else {
            System.out.println("All ready exist");
        }
    }
}
