package my.home.project.test_prj.position;

import my.home.project.test_prj.ListDB;
import my.home.project.test_prj.dao.PostionDao;

import java.util.ArrayList;
import java.util.Scanner;

public class DeletePosition {
    public static void deletePosition(){

        PostionDao positionDao = new PostionDao();
        Scanner scanner = new Scanner(System.in);

        if (positionDao.geAll().isEmpty()) {
            System.out.println("list is empty");
            return;
        }
        InfoPosition.showInfo();
        System.out.println("Enter ID you wont to delete:  1 - " + positionDao.geAll().size());
        int id = scanner.nextInt() - 1;
        System.out.println("user " + positionDao.geAll().get(id).getJobTitle() + " has been deleted " );
        positionDao.delete(positionDao.geAll().get(id));
    }
}
