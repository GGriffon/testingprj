package my.home.project.test_prj.position;

import my.home.project.test_prj.ListDB;
import my.home.project.test_prj.dao.PostionDao;

import java.util.ArrayList;
import java.util.Scanner;

public abstract class CreatePosition {
    private static int count = 0;

    public static void createPosition() {
        PostionDao positionDao = new PostionDao();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter JT");
        String JT = scanner.next();
        if (positionDao.geAll().stream().noneMatch(position -> position.getJobTitle().contains(JT))) {
            count++;
            Position position = new Position(count, JT);
            positionDao.create(position);
            System.out.println("Position - " + JT + " has been created");
        } else {
            System.out.println("All ready exist");
        }
    }

    public static void createPosition(String JT) {
        PostionDao positionDao = new PostionDao();
        if (positionDao.geAll().stream().noneMatch(position -> position.getJobTitle().contains(JT))) {
            count++;
            Position position = new Position(count, JT);
            positionDao.create(position);
        }
    }
}
