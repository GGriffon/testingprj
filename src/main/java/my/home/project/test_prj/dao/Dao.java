package my.home.project.test_prj.dao;

import java.util.List;

public interface Dao<T> {
    List<T>geAll();
    void create(T t);
    Object read(int id);
    void update(T t, String[] param);
    void delete(T t);
}
