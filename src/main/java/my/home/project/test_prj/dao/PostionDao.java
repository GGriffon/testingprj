package my.home.project.test_prj.dao;

import my.home.project.test_prj.position.Position;

import java.util.ArrayList;
import java.util.List;

public class PostionDao implements Dao<Position> {
    private static final List<Position>positions = new ArrayList<>();
    @Override
    public List<Position> geAll() {
        return positions;
    }

    @Override
    public void create(Position position) {
        positions.add(position);
    }

    @Override
    public Object read(int id) {
        return positions.get(id);
    }

    @Override
    public void update(Position position, String[] param) {
        position.setJobTitle(param[0]);
        //positions.add(position);
    }

    @Override
    public void delete(Position position) {
        positions.remove(position);
    }
}
