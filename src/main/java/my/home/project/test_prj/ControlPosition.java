package my.home.project.test_prj;

import my.home.project.test_prj.position.CreatePosition;
import my.home.project.test_prj.position.DeletePosition;
import my.home.project.test_prj.position.InfoPosition;
import my.home.project.test_prj.position.ModifyPosition;

import java.util.Scanner;

public class ControlPosition {

    public void controlPosition() {

        Scanner scanner = new Scanner(System.in);
        String userInput = "";
        while (!userInput.equals("Q")) {
            System.out.println("Q - quit, C - create, M - modify, D - delete, I - info");
            userInput = scanner.next();
            userInput = userInput.toUpperCase();

            switch (userInput) {
                case "Q": {
                    System.out.println("Работа завершина");
                    break;
                }
                case "C": {
                    CreatePosition.createPosition();
                    break;
                }
                case "M": {
                    ModifyPosition.modifyPosition();
                    break;
                }
                case "D": {
                    DeletePosition.deletePosition();
                    break;
                }
                case "I": {
                    InfoPosition.showInfo();
                    break;
                }
                default: {
                    System.out.println("pick from the list ");
                }
            }
        }
    }
}
