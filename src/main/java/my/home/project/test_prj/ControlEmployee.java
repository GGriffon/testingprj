package my.home.project.test_prj;

import my.home.project.test_prj.employee.*;
import my.home.project.test_prj.filter.FilterEmployee;

import java.util.Scanner;

public class ControlEmployee {
    public void controlEmployee() {
        Scanner scanner = new Scanner(System.in);
        String userInput = "";
        while (!userInput.equals("Q")) {
            System.out.println("Q - quit, C - create, M - modify, D - dismiss, I - info, S - sort, F - filter");
            userInput = scanner.next();
            userInput = userInput.toUpperCase();

            switch (userInput) {
                case "Q": {
                    System.out.println("Работа завершина");
                    break;
                }
                case "C": {
                    CreateEmployee.createEmployee();
                    break;
                }
                case "M": {
                    ModifyEmployee.modifyEmployee();
                    break;
                }
                case "D": {
                    DismissEmployee.dismissEmployee();
                    break;
                }
                case "I": {
                    InfoEmployee.showInfo();
                    break;
                }
                case "S": {
                    SortEmployee.sortEmployee();
                    break;
                }
                case "F": {
                    FilterEmployee.filterEmployee();
                    break;
                }
                default:
                    System.out.println("Wrong pick");
            }
        }
    }
}
